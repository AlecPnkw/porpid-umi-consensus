"""
Usage: julia UMI-processing.jl CCS.fastq primer-sheet.csv

This julia script takes an input CCS.fastq file along with a primer sheet specifying
the PCR design, producing single template consensus sequences via the PORPID method.
"""

#load required packages
include("UMI-methods.jl")

#read in sample sheet, filter to UMI protocol
primer_sheet = @linq CSV.read(ARGS[2]) |>
    where(:UMI .!= "NA")

#filter .fastq
println("Filtering .fastq file...")
@time fastq_filter(ARGS[1],
                   "filtered.fastq",
                   error_rate = 0.01,
                   min_length = 2100,
                   max_length = 4000)

#load sequences
println("Reading filtered sequences...")
seqs,phreds,seq_names = read_fastq("filtered.fastq")

#demux, writing to named .fastq files
mkdir("demux")
t1 = time()

#forward
fwd_ends = collect(primer_sheet[!, :Forward_Primer_2ndRd_Sequence]) #select full length
unique_fwd_ends = unique_not_substr(fwd_ends)

fwd_end_group_arr = []
for e in fwd_ends
    for (j, group) in enumerate(unique_fwd_ends)
        if occursin(e, group)
            push!(fwd_end_group_arr, j)
        end
    end
end
#fwd_end_dic = Dict([s => i for (i,s) in enumerate(unique_fwd_ends)])
#fwd_end_group_arr = [fwd_end_dic[e] for e in fwd_ends]

println("Splitting by primers...")
rev_adapter = unique(primer_sheet[!, :Reverse_Primer_2ndRd_Sequence])[1] #set universal reverse adapter

#sliding window demultiplex on forward primers
fwd_demux_dic = sliding_demux_dict(seqs,
                                   unique_fwd_ends,
                                   12,
                                   13,
                                   verbose=false,
                                   phreds = phreds)

#iterate by each forward primer group
for j in 1:length(unique_fwd_ends)
    analyzing = fwd_end_group_arr .== j

    #define templates
    template_names = primer_sheet[analyzing, :Sample]
    sampleIDs = primer_sheet[analyzing, :Sample_ID]
    IDind2name = Dict(zip(collect(1:length(sampleIDs)),template_names));
    println("Template names and ID ix: $(IDind2name)")
    println("Sample IDs: $(sampleIDs)")

    #retrieve from demux_dic
    seqs_fwd = [i[1] for i in fwd_demux_dic[j]];
    phreds_fwd = [i[2] for i in fwd_demux_dic[j]];
    seq_names_fwd = seq_names[[i[3] for i in fwd_demux_dic[j]]]

    println("$(length(seqs_fwd)) reads matching forward primer $(unique_fwd_ends[j])")

    #match to reverse adapter
    rev_matches = iterative_primer_match(seqs_fwd, [rev_adapter], 12, 13, tol_one_error=true);
    rev_keepers = rev_matches .< 0

    #filter to reverse adapter matches
    seqs_both = seqs_fwd[rev_keepers]
    phreds_both = phreds_fwd[rev_keepers]
    seq_names_both = seq_names_fwd[rev_keepers]

    println("$(length(seqs_both)) contain reverse adapter $(rev_adapter)")

    #determine full primer sequences (necessary?)
    if primer_sheet[analyzing, :UMI][1] == "dUMI"
    fwd_primer = primer_sheet[analyzing, :Second_Strand_Primer_Sequence][1] #trims past 2nd UMI
    else
    fwd_primer = unique_fwd_ends[j]
    end

    #trim and orient primers, phreds
    println("Trimming primers for read group $(j)...")
    trimmed = [double_primer_trim(seqs_both[i],
                                        phreds_both[i],
                                        fwd_primer,
                                        rev_adapter) for i in 1:length(seqs_both)]

    #separate by donor (sample) IDs
    println("Splitting read group $(j) by donor ID...")
    split_donor_dic = demux_dict([s[1] for s in trimmed],
                                       sampleIDs,
                                       nothing,
                                       verbose=false,
                                       phreds=[s[2] for s in trimmed],
                                       tol_one_error=false)

    println("Writing individual donor files...")
    for i in 1:length(sampleIDs)
        println(IDind2name[i]," => ",length(split_donor_dic[i]))
        template = template_names[i]
        write_fastq("demux/"*template*".fastq",
                    [i[1] for i in split_donor_dic[i]],
                    [i[2] for i in split_donor_dic[i]];
                    names = seq_names_both[[i[3] for i in split_donor_dic[i]]])
    end
end

t2 = time()
println("Demultiplex took $(t2 - t1) seconds.")

#iterate through samples, run PORPID, and filter families.
t1 = time()

tag_dfs = []
for i in 1:size(primer_sheet,1)
    data_dir = "demux"
    ID = primer_sheet[i, :Sample_ID]
    umi_ix = findfirst(r"N+", primer_sheet[i, :sUMI_Primer_Sequence])
    #trying template with 6bp sample IDs and last 8bp primer
    template_suffix = replace(primer_sheet[i, :sUMI_Primer_Sequence][umi_ix[1]:end],
                       "N" => "n")*"*"

    filtered_data_file = primer_sheet[i, :Sample]*".fastq"
    templates = Dict()
    templates[primer_sheet[i, :Sample]] = ID*template_suffix

    cfg = Configuration()
    cfg.files = ["$(data_dir)/$(filtered_data_file)"]
    cfg.filetype = fastq
    cfg.start_inclusive = 0
    cfg.end_inclusive = 6 + length(template_suffix) + 2 #setting this explicitly
    cfg.try_reverse_complement = false #The sequences are already oriented
    for (name, template) in templates
        push!(cfg.templates, Template(name, template))
    end

    println("Processing output/$(filtered_data_file)/...")
    println("Using template $(ID*template_suffix)")
    if isdir("output/$(filtered_data_file)/")
        @error "The directory already exists! Please delete"
        #rm("output/$(filtered_data_file)/", recursive=true)
    end
    dir_dict = Dict()
    my_output_func(source_file_name,
                   template,
                   tag,
                   output_sequence,
                   score
    ) = PORPID.write_to_file_count_to_dict(dir_dict,
                                           source_file_name,
                                           template,
                                           tag,
                                           output_sequence,
                                           score)
    say_print_func = function(count)
        println("Processed $(count) sequences")
    end
    # This is the slow bit
    extract_tags_from_file(cfg.files[1],
                           cfg,
                           my_output_func,
                           print_every=5000,
                           print_callback=say_print_func)
    directories = collect(keys(dir_dict))

    #There is now only one template per demuxed dataset
    analysing_template = 1;
    template_name = basename(directories[analysing_template])
    tag_dict = dir_dict[directories[analysing_template]]
    delete!(tag_dict, "REJECTS")
    tag_counts = tag_dict
    tags = collect(keys(tag_dict))

    #Builts matrix of conditional probabilities.
    tag_to_index, index_to_tag = tag_index_mapping(tags)
    pacbio_error_rate = 0.005
    recurse = 1
    probabilities_array = prob_observed_tags_given_reals(tag_to_index,
                                                         PORPID.PacBioErrorModel(pacbio_error_rate),
                                                         recurse)
    indexed_counts = index_counts(tag_counts, tag_to_index);
    #Runs the LDA inference
    most_likely_real_for_each_obs = LDA(probabilities_array, indexed_counts)
    path = "output/$(filtered_data_file)/"*template_name*"/"

    #Filter and copy to "_keeping"
    tag_df = filterCCSFamilies(most_likely_real_for_each_obs, path, index_to_tag, tag_counts, template_name, templates[template_name])
    push!(tag_dfs, tag_df)
end

CSV.write("output/family_tags.csv", sort!(vcat(tag_dfs...), [:Sample, :tags, :fs], rev = (false, false, true))); # ... necessary to collapse cols
t2 = time()
println("UMI identification took $(t2-t1) seconds.")

#Calculate consensus sequences for each family.
t1 = time()
if isdir("consensus/")
    @error "The directory already exists! Please delete"
    else mkdir("consensus/")
end
for i in 1:size(primer_sheet,1)
    template_name = primer_sheet[i, :Sample]
    to_trim = primer_sheet[i, :sUMI_Primer_Sequence][26:end] #trims from SID on
    println("Processing $(template_name)")
    direc = template_name*".fastq"
    base_dir = "output/"*direc*"/"*template_name*"_keeping"
    @time seq_collection, seqname_collection = generateConsensusFromDir(base_dir, template_name)
    trimmed_collection = [primer_trim(s,to_trim) for s in seq_collection];
    write_fasta("consensus/"*template_name*".fasta",reverse_complement.(trimmed_collection),names = seqname_collection)
end
t2 = time()
println("Consensus generation took $(t2-t1) seconds.")
