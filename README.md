# H704-PORPID-UMI-pipeline

Processes PacBio CCS tagged with unique molecular identifiers (UMIs) using the Probabilistic Offspring Resolver for Primer IDs (PORPID), producing single-template consensus sequences. Configured to work with data produced as part of HVTN704 in the Mullins Lab.

## Usage

```bash
julia UMI-processing.jl CCS.fastq primer-sheet.csv
```

Where `CCS.fastq` contains circular consensus sequences in any orientation and `primer-sheet.csv` is a table containing information regarding PCR amplicon design. The table should have the following columns:

- Sample                       
- Target                       
- Project                      
- UMI                          
- Scientist                    
- sUMI_Primer                  
- Second_Strand_Primer         
- Forward_Primer_2ndRd         
- Reverse_Primer_2ndRd         
- N7_Index                     
- S5_Index                     
- N7_Index_Sequence            
- S5_Index_Sequence            
- Sample_ID                    
- sUMI_Primer_Sequence         
- Second_Strand_Primer_Sequence
- Forward_Primer_2ndRd_Sequence
- Reverse_Primer_2ndRd_Sequence

## Installation

First, install Julia: https://julialang.org/downloads/. The scripts are currently set up to work with v1.2.

Next, install the following packages in the Julia REPL. Julia might complain about the package DPMeansClustering, but you can install following the instructions here and try again: https://github.com/MurrellGroup/DPMeansClustering.jl. 

```julia
using Pkg
Pkg.add(PackageSpec(name="NextGenSeqUtils", rev="1.0", url = "https://github.com/MurrellGroup/NextGenSeqUtils.jl.git"))
Pkg.add(PackageSpec(name="DPMeansClustering", rev="1.0", url = "https://github.com/MurrellGroup/DPMeansClustering.jl.git"))
Pkg.add(PackageSpec(name="RobustAmpliconDenoising", rev="1.0", url = "https://github.com/MurrellGroup/RobustAmpliconDenoising.jl.git"))
Pkg.add(PackageSpec(name="PORPID", rev="master", url = "https://github.com/MurrellGroup/PORPID.jl.git"))
Pkg.add("PyPlot")
Pkg.add("StatsBase")
Pkg.add("HypothesisTests")
Pkg.add("IterTools")
Pkg.add("DataFrames")
Pkg.add("DataFramesMeta")
Pkg.add("CSV")
Pkg.add("Distributions")
```

Note 1: If you run into an error with PyCall, you likely need to follow the instructions in the error message:
```julia
ENV["PYTHON"]=""
using Pkg
Pkg.build("PyCall")
```
Note 2: multiple threads can be used by a flag:
```julia
julia -p 8 <rest of command>
```
This distributes the consensus call. It has a large once-off computational cost.

